# -*- coding: utf-8 -*-

import requests
import pandas as pd
import os
import time

#'http://tisvcloud.freeway.gov.tw/history/TDCS/M06A/20191119/23/TDCS_M06A_20191119_230000.csv'
TDCS_M06A = "http://tisvcloud.freeway.gov.tw/history/TDCS/M06A/"
Year = "2021"
Month = "04"

Day = 9 #開始日期
end_day = 16 #結束日期

Hour_Start = 0 #開始時間
Hour_End = 23 #結束時間


OutPutDir = "./TDCS_Download/M06A/"+Year+Month
#os.mkdir(OutPutDir)
if not (os.path.exists(OutPutDir)):
    os.makedirs(OutPutDir)
    print("Dir Created "+OutPutDir)
else:
    print("Dir Existed "+OutPutDir)


for daily in range(Day, end_day+1):
    if daily < 10:
        DayStr = "0"+str(daily)
        print("0"+str(daily))
    else:
        DayStr = str(daily)
        print (str(daily))
    for Hour in range(Hour_Start, Hour_End+1):
        if Hour < 10:
            HourStr = "0"+str(Hour)
            print("0"+str(Hour))
        else:
            HourStr = str(Hour)
            print (str(Hour))
        #Hour = 23
        OneDate = Year+Month+DayStr
        OneDay_OneHour_FileName = "TDCS_M06A_"+OneDate+"_"+HourStr+"0000.csv"
        url = TDCS_M06A +OneDate+'/'+HourStr+'/'+ OneDay_OneHour_FileName
        print(url)
        csv_data = pd.read_csv(url,encoding='utf-8')
        
        OutputFileLocation = OutPutDir+"/"+OneDay_OneHour_FileName
        #OutputFileLocation
        csv_data.to_csv(OutputFileLocation)
        print("Download=>"+OutputFileLocation)
        time.sleep(1)




    ########################
    #####   ANCRAFT    #####
    ########################