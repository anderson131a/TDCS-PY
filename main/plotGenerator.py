# -*- coding: utf-8 -*-
import pandas as pd
import matplotlib.pyplot as plt
import yaml
with open('./config/plot.yml' ,'r') as config: #開啟設定檔
    y = yaml.safe_load(config.read()) #讀取設定檔
    timee = y['time'] #圖表結束日期設定(20210403)
    title = y['title'] #輸出的檔案標題
    filename = y['filename'] #輸出的檔案名稱
    txtname = y['txtname'] #數據來源的名稱(txtanc)
    xtilte = y['xtilte'] #x軸標題
'''
Date	Hour	GantryID	31	32	41	42	5
20210415	00	03F2100S	59	13	1	1	1
20210415	01	03F2100S	44	9	1	6	0
20210415	02	03F2100S	29	6	1	6	1
20210415	03	03F2100S	14	7	0	3	5
20210415	04	03F2100S	35	10	0	8	7
''' #檔案樣式範例^

TDCS_Data = pd.read_csv("./data/"+txtname+".txt", sep='\t', header=0) #引入檔案
time1 = timee

OneDay_Date = TDCS_Data[TDCS_Data['Date']<= time1]
OneDay_Date_Hour = TDCS_Data[TDCS_Data['Date']<= time1]['Hour']
OneDay_Date_VT_31 = TDCS_Data[TDCS_Data['Date']<= time1]['31']
OneDay_Date_VT_32 = TDCS_Data[TDCS_Data['Date']<= time1]['32']
OneDay_Date_VT_41 = TDCS_Data[TDCS_Data['Date']<= time1]['41']
OneDay_Date_VT_42 = TDCS_Data[TDCS_Data['Date']<= time1]['42']
OneDay_Date_VT_5 = TDCS_Data[TDCS_Data['Date']<= time1]['5']


plt.figure(figsize=(12,8))

plt.bar(OneDay_Date_Hour,OneDay_Date_VT_31, label='VT_31', width=0.25)
plt.bar(OneDay_Date_Hour,OneDay_Date_VT_32, label='VT_32', width=0.25)
plt.bar(OneDay_Date_Hour,OneDay_Date_VT_41, label='VT_41', width=0.25)
plt.bar(OneDay_Date_Hour,OneDay_Date_VT_42, label='VT_42', width=0.25)
plt.bar(OneDay_Date_Hour,OneDay_Date_VT_5, label='VT_5', width=0.25)
plt.legend()  

plt.title(title,fontsize=14)
plt.xlabel(xtilte,fontsize=12)
plt.ylabel('Count',fontsize=12)
plt.grid()

plt.savefig('./Output/' + filename + '.jpg') #存為圖檔

plt.show()




    ########################
    #####   ANCRAFT    #####
    ########################
