import csv
import os
import yaml
with open('./config/txt.yml' ,'r') as config: #開啟設定檔
    y = yaml.safe_load(config.read()) #讀取設定檔
    GID = y['GantryID'] #寫入變數
    name = y['Name'] #寫入變數
list = 0 #一種記分板
for file in os.listdir('./TDCS_Download/M06A/202104/'):
    #######載入資料

    InputDir = "./TDCS_Download/M06A/202104/" #路徑
    InputFileName = file #檔案

    fn = os.path.join(InputDir, InputFileName) #指向目標文件路徑

    #######資料處理

    d = {"31":0, "32":0, "41":0, "42":0, "5":0, "0":0} #一種minecraft記分板
    GantryID = GID #尋找的目標(現在到txt.yml中設定)
    VT = 0

    with open(fn) as file: #檔案開啟
        for line in file: #逐行處理
            Items = line.split(',') #分隔單格資料
            if (Items[3].find(GantryID) != -1): #檢查有無找到目標
                VT = Items[1] #儲存資料
                d[VT] = d[VT] + 1 #儲存到記分板上

    #######寫入資料


    OutPutDir = "./data"  #資料夾位置
    if (os.path.exists(OutPutDir)):
        print("Existed => "+OutPutDir)
    else:  #資料夾不存在
        os.mkdir(OutPutDir) #創建資料夾
        print("Create => "+OutPutDir)

    list = list + 1 #加一
    ID = name #檔名
    OutputfileName = InputFileName[:25]
    Date = InputFileName[10:18]
    Hour = InputFileName[19:21]+"_"+str(list) #為小時加上編號
    Version = "V5"
    fn = os.path.join(OutPutDir, ID+".txt")

    #將資料寫進檔案
    if (os.path.exists(fn)): #檔案已經存在
        with open(fn, 'a') as file_Obj:
            file_Obj.write(Date+"\t")
            file_Obj.write(Hour+"\t")
            file_Obj.write(GantryID+"\t")
            file_Obj.write(str(d["31"])+"\t")
            file_Obj.write(str(d["32"])+"\t")
            file_Obj.write(str(d["41"])+"\t")
            file_Obj.write(str(d["42"])+"\t")
            file_Obj.write(str(d["5"])+"\n")
    else: #檔案尚未創建
        with open(fn, 'w') as file_Obj:
            file_Obj.write("Date"+"\t"+"Hour"+"\t"+"GantryID"+"\t"+"31"+"\t"+"32"+"\t"+"41"+"\t"+"42"+"\t""5"+"\n") #標頭

            file_Obj.write(Date+"\t")
            file_Obj.write(Hour+"\t")
            file_Obj.write(GantryID+"\t")
            file_Obj.write(str(d["31"])+"\t")
            file_Obj.write(str(d["32"])+"\t")
            file_Obj.write(str(d["41"])+"\t")
            file_Obj.write(str(d["42"])+"\t")
            file_Obj.write(str(d["5"])+"\n")




    ########################
    #####   ANCRAFT    #####
    ########################
