# 進階程式設計
1. txtGenerator 是用來將資料整理為一份txt檔案
2. Downloader 是資料下載器
3. plotGenerator 是圖表生成器

## 1.txtGenerator
---
利用Downloader下載的CSV資料，讀取並挑選出需要的車次數據。且按照車種進行分類統計
#### 15行:設定目標ID
```python=
    GantryID = "03F2100N" #尋找的目標
```
更改交流道id 系統會統計經過此ID的車種與數量

#### 20行:指定搜尋的欄位
```python=
            if (Items[3].find(GantryID) != -1): #檢查字元
                VT = Items[1] #儲存資料
                d[VT] = d[VT] + 1 #結算
```
將Items[3]改為你想指定的位置(上交流道或下交流道)
> 使用Items[0]可以讀取 Traffic欄位的數據
> 使用Items[1]可以讀取 VehicleType欄位的數據
> 使用Items[2]可以讀取 GantryTo欄位的數據

![](https://i.imgur.com/MnmSkcL.png)

#### 43行:txt輸出格式
```python=

    #將資料寫進檔案
    if (os.path.exists(fn)): #檔案已經存在
        with open(fn, 'a') as file_Obj:
            file_Obj.write(Date+"\t")
            file_Obj.write(Hour+"\t")
            file_Obj.write(GantryID+"\t")
            file_Obj.write(str(d["31"])+"\t")
            file_Obj.write(str(d["32"])+"\t")
            file_Obj.write(str(d["41"])+"\t")
            file_Obj.write(str(d["42"])+"\t")
            file_Obj.write(str(d["5"])+"\n")
    else: #檔案尚未創建
        with open(fn, 'w') as file_Obj:
            file_Obj.write("Date"+"\t"+"Hour"+"\t"+"GantryID"+"\t"+"31"+"\t"+"32"+"\t"+"41"+"\t"+"42"+"\t""5"+"\n") #標頭

            file_Obj.write(Date+"\t")
            file_Obj.write(Hour+"\t")
            file_Obj.write(GantryID+"\t")
            file_Obj.write(str(d["31"])+"\t")
            file_Obj.write(str(d["32"])+"\t")
            file_Obj.write(str(d["41"])+"\t")
            file_Obj.write(str(d["42"])+"\t")
            file_Obj.write(str(d["5"])+"\n")

```
將`\t`更換為`，`可以匯入EXCEL

## 2.Downloader
---
將預定範圍的資料從`tisvcloud.freeway.gov.tw`各類車種旅次數量庫下載並儲存，可設定路徑放置檔案
#### 18行:設定下載範圍
```python
Day = 1 #開始日期
end_day = 8 #結束日期

Hour_Start = 0 #開始時間
Hour_End = 23 #結束時間
```
設定檔案下載從幾號開始

#### 25行:檔案儲存位置
```python
OutPutDir = "./TDCS_Download/M06A/"+Year+Month
```
設定儲存路徑，可以選擇下載的檔案放置位置






## 3.plotGenerator
---
將txtGenerator產生的數據繪製成圖表，並儲存到指定位置
![](https://i.imgur.com/K3344US.png)

#### 14行:讀取檔案
```python
TDCS_Data = pd.read_csv("./data/txtanc.txt", sep='\t', header=0) #引入檔案
```
設定讀取的檔案路徑及分隔方式

#### 15行:指定日期
```python
time1 = 20210401
```
指定圖表資料的日期，作為繪製範圍